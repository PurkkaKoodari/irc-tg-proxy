#!/usr/bin/python3

from telegram import telegram
import base64, datetime, getpass, hashlib, json, logging, os, random, re, socket, sys, threading, time
import autorestart

format = "%(asctime)s [%(name)s] [%(levelname)s] %(message)s"
logging.basicConfig(stream = sys.stdout, level = logging.INFO, format = format)
tlogger = logging.getLogger("IRCRelay-TG")
ilogger = logging.getLogger("IRCRelay-IRC")

tlogger.info("Loading configuration...")
try:
    if len(sys.argv) > 1:
        configfile = sys.argv[1]
    else:
        configfile = "ircrelaybot.config.json"
    with open(configfile) as conf:
        config = json.load(conf)
        for key in ["tguser", "token", "server", "port", "channel", "botnicks", "realname", "username", "allowimages", "imagepath", "urlpath"]:
            locals()[key] = config[key]
except KeyError as e:
    tlogger.fatal("Missing configuration key: %s" % e.args[0])
    exit(1)
except:
    tlogger.fatal("Failed to read configuration file. Ensure that a readable %s exists in the current folder." % configfile)
    exit(1)

shutting_down = False

def shutdown(shutdown = False):
    """Shuts down the client.

    shutdown should be True if this is not caused by a restart.

    If already shutting down and not restarting, enter an infinite loop.

    Otherwise, send a quit message to Telegram and ask the auxiliary threads to die (which will in turn send a quit message to IRC)."""
    global shutting_down
    if shutting_down:
        while shutdown:
            time.sleep(1)
        return

    shutting_down = True

    tgthread.queue({
        "chat_id": tguser,
        "text": "<i>Bot shutting down.</i>" if shutdown else "<i>Bot restarting.</i>",
        "parse_mode": "HTML",
    })

    tlogger.info("Shutting down IRC thread...")

    ircthread.please_die(not shutdown)
    tgthread.please_die()
    ircthread.join()
    ircthread.kill_sender()

    tlogger.info("Shutting down Telegram thread... (may Ctrl-C now)")

    try:
        tgthread.join()
        tgthread.kill_sender()
    except KeyboardInterrupt:
        pass

# monitor bot files using autorestart
autorestart.monitor(__file__, configfile, cleanup = shutdown)

# get the user's username if none given
username = username or getpass.getuser()

# create the bot
assert token != ""
bot = telegram.Bot(token)

# confirm Telegram token
tlogger.info("Confirming token...")
bot.confirmToken()
tlogger.info("Token confirmed.")
# skip any Telegram updates to avoid spamming IRC
tlogger.info("Getting current update ID...")
bot.clearUpdates()
tlogger.info("Got current update ID.")

def parse_irc(ircmsg):
    """Parses the parts of an IRC message.

    Returns (sender, senderuser, senderhost, command, chan, args, msg), where
     - sender is the sender's IRC nickname or ''
     - senderuser is the sender's username or ''
     - senderhost is the sender's hostname or ''
     - command is the IRC command
     - chan is the first argument or ''
     - args is the array of all arguments
     - msg is the last argument or ''"""
    # split sender part
    if ircmsg.startswith(":") and " " in ircmsg:
        sndpart, msgpart = ircmsg.split(" ", 1)
    else:
        sndpart, msgpart = "", ircmsg
    # sender
    if "@" in sndpart:
        sndpart, senderhost  = sndpart.split("@", 1)
    else:
        senderhost = ""
    if "!" in sndpart:
        sndpart, senderuser = sndpart.split("!", 1)
    else:
        senderuser = ""
    sender = sndpart[1:]
    # split command
    index = 0
    args = []
    while index < len(msgpart):
        if msgpart[index] != ":" and " " in msgpart[index:]:
            arg = msgpart[index:msgpart.index(" ", index)]
            index += len(arg) + 1
        else:
            arg = msgpart[index:] if msgpart[index] != ":" else msgpart[index + 1:]
            index = len(msgpart)
        if arg:
            args.append(arg)
    # special command parts
    command, *args = args if args else ("",)
    chan = args[0] if args else ""
    msg = args[-1] if args else ""
    return sender, senderuser, senderhost, command, chan, args, msg

def irclower(text):
    """Changes text into lowercase according to the IRC standard.

    In addition to changing cased characters to lowercase, turns []\\~ to {}|^ respectively."""
    return text.lower().translate({91: "{", 93: "}", 92: "|", 94: "^"})

def htmlencode(data):
    """Encodes a message with HTML entities for sending to Telegram.

    Escapes <, > and & to &lt;, &gt;, &amp; respectively."""
    return data.translate({38: "&amp;", 60: "&lt;", 62: "&gt;"})

def plurals(amount, unit, suffix = "s"):
    """Formats a plurals expression.

    Suffix is formatted into unit if not singular, and amount is prepended."""
    return str(amount) + unit % (suffix if amount != 1 else "")

def ping_timestamp(date):
    """Formats a date for a ping timestamp.

    The result includes the absolute date if different from current, the absolute time, and the relative time."""
    now = datetime.datetime.now()
    date = datetime.datetime.fromtimestamp(date)
    # show only time if same day (in host timezone), otherwise date + time
    if date.date() == now.date():
        absolute = date.strftime("%H:%M:%S")
    else:
        absolute = date.strftime("%-d.%-m.%Y %H:%M:%S")
    # calculate time offset from now
    diff = now - date
    if diff.days >= 1:
        relative = plurals(diff.days, " day%s ago")
    elif diff.seconds >= 3600:
        relative = plurals(diff.seconds // 3600, " hour%s ago")
    elif diff.seconds >= 60:
        relative = plurals(diff.seconds // 60, " minute%s ago")
    else:
        relative = plurals(diff.seconds, " second%s ago")
    return "%s (%s)" % (absolute, relative)

class queue_consumer_thread(threading.Thread):
    def __init__(self):
        super().__init__()
        self._queue = []
        self._cond = threading.Condition()
        self._running = True
    def queue(self, message):
        """Queue a message for the sender."""
        self._cond.acquire()
        self._queue.append(message)
        self._cond.notify()
        self._cond.release()
    def please_die(self):
        """Tell the sender to asynchronously kill itself."""
        self._cond.acquire()
        self._running = False
        self._cond.notify()
        self._cond.release()
    def run(self):
        while self._running:
            with self._cond as _:
                self._cond.wait_for(lambda: len(self._queue) > 0 or not self._running)
                for message in self._queue:
                    try:
                        self.consume(message)
                    except:
                        tlogger.error("Error while sending message.", exc_info = 1)
                self._queue.clear()

class irc_send_thread(queue_consumer_thread):
    INITIALIZING = 0 # Status when no connection exists
    CONNECTED = 1    # Status when the socket has connected, but the welcome message has not been received
    ACCEPTED = 2     # Status when the welcome message has been received, but no channels have been joined
    JOINED = 3       # Status when the initial channels have been joined
    PING = 0         # Priority for messages which will be dropped if no connection exists
    JOINING = 1      # Priority for messages which are needed in order to get the welcome message
    INTERNAL = 2     # Priority for messages after the welcome message that can be dropped
    USER = 3         # Priority for messages after the welcome message that can't be dropped
    def __init__(self, lock):
        super().__init__()
        self._ircsock = None
        self._lock = lock
        self._droplog = []
        self._waiting = {
            irc_send_thread.CONNECTED: [],
            irc_send_thread.ACCEPTED: [],
            irc_send_thread.JOINED: []
        }
        self.status = irc_send_thread.INITIALIZING
    def queue(self, message):
        super().queue(message)
        contents, priority = message
        if priority == irc_send_thread.USER:
            self._droplog.append(message)
    def clear_drop_log(self):
        """Clear the log of possibly dropped important messages.

        This should be called when the server responds to a ping, essentially confirming the
        success of previous messages."""
        self._droplog.clear()
    def reset(self):
        """Reset the sender: clear the queue and change state to INITIALIZING. Also adds possibly
        dropped messages to the user wait queue and clears other wait queues.geogebra

        This should be followed by the initialization of a new socket and a call to
        change_socket()."""
        self.status = irc_send_thread.INITIALIZING
        self._queue.clear()
        self._waiting.get(irc_send_thread.USER, []).extend(self._droplog)
        self._droplog.clear()
    def change_socket(self, newsock):
        """Change the socket the sender uses to send messages, and change state to CONNECTED."""
        self._ircsock = newsock
        self.status = irc_send_thread.CONNECTED
    def update_status(self, status):
        """Update the status of the sender and queue all messages left waiting for an update."""
        self.status = status
        for level in range(status + 1):
            waits = self._waiting.get(level, [])
            self._queue.extend(waits)
            waits.clear()
    def consume(self, message):
        """Consume a message by sending it to IRC.

        If the status is too early to send that message, it will dropped or queued until a call of
        update_status() with a high enough status. See the priority constants for details."""
        contents, priority = message
        if priority < irc_send_thread.JOINING and self.status < irc_send_thread.CONNECTED:
            return
        elif self.status < priority < irc_send_thread.USER or self.status < irc_send_thread.ACCEPTED and priority == irc_send_thread.USER:
            self._waiting.get(priority, []).append(message)
        else:
            with self._lock as _:
                self._ircsock.send(bytes(contents, "utf-8") + b"\r\n")

class tg_send_thread(queue_consumer_thread):
    def __init__(self, bot):
        super().__init__()
        self.pings = []
        self.messageid = 0
        self.ping_target = None
    def consume(self, message):
        if message["ping"] is True and self.ping_target is not None:
            message["message"]["reply_to_message_id"] = self.ping_target
        response = bot.doRequest("sendMessage", message["message"])
        if message["ping"] is True:
            content = message["message"]["text"] if "text" in message["message"] else None
            self.pings.append({
                "content": content,
                "time": time.time(),
                "ircid": self.messageid,
                "tgid": response["message_id"],
            })
        if message["pingable"]:
            self.ping_target = response["message_id"]
        if message["ping"] is not None:
            self.messageid += 1

class irc_ping_thread(threading.Thread):
    def __init__(self, main):
        super().__init__()
        self._main = main
        self._running = True
        self._cond = threading.Condition()
        self.responded = True
    def please_die(self):
        self._cond.acquire()
        self._running = False
        self._cond.notify()
        self._cond.release()
    def run(self):
        while self._running:
            with self._cond as _:
                self._cond.wait_for(lambda: not self._running, 120)
            if not self.responded:
                ilogger.warn("Ping timeout. Reconnecting.")
                tgthread.queue({
                    "chat_id": tguser,
                    "text": "<i>Ping timeout. Reconnecting.</i>",
                    "parse_mode": "HTML",
                })
                self._main.reconnect()
                self.responded = True
            elif self._main.sender.status >= irc_send_thread.CONNECTED:
                ilogger.info("Sending PING to server")
                self._main.sender.queue(("PING :" + server, irc_send_thread.PING))
                self.responded = False

class irc_thread(threading.Thread):
    def __init__(self):
        super().__init__()
        self._running = True # True if the thread should keep running
        self._ircsock = None # The IRC socket
        self.botnick = None # The accepted nick for the bot
        self._leavereason = "Bot shutting down." # The leave reason sent on disconnect
        self._socklock = threading.Lock() # The lock for using the socket
        self.sender = irc_send_thread(self._socklock) # The sender thread
        self.pinger = None # The pinger thread
        self._retry = [0, 0] # The number of times all usernames have been tried, and the index to try
        self.whoisdata = {} # Whois data for each user
        self._restarting = False
    def queue(self, message):
        for row in message.split("\n"):
            if row:
                ilogger.info("[" + channel + "] <" + self.botnick + "> " + row)
                self.sender.queue(("PRIVMSG " + channel + " :" + row, irc_send_thread.USER))
    def queue_raw(self, data, priority):
        self.sender.queue((data, priority))
    def please_die(self, restart = True):
        if restart:
            self._leavereason = "Bot restarting."
        self._running = False
        if self._ircsock:
            self._ircsock.shutdown(socket.SHUT_RD)
    def kill_sender(self):
        self.sender.please_die()
        self.sender.join()
    def kill_pinger(self):
        self.pinger.please_die()
        self.pinger.join()
    def setnick(self):
        newnick = botnicks[self._retry[0]]
        ilogger.info("Trying username " + newnick + "...")
        self.sender.queue(("NICK " + newnick, irc_send_thread.JOINING))
        return newnick
    def reconnect(self):
        if not self._running:
            return
        if self._restarting:
            while self._restarting:
                time.sleep(0.1)
            return
        self._restarting = True
        self.sender.reset()
        if self._ircsock:
            self.sender.consume(("QUIT :Bot reconnecting.", irc_send_thread.PING))
            ilogger.info("Closing connection.")
            try:
                self._ircsock.shutdown(socket.SHUT_RDWR)
                self._ircsock.close()
            except:
                pass
            ilogger.info("Reconnecting in 2 seconds.")
            time.sleep(2)
        with self._socklock as _:
            ilogger.info("Connecting to " + server + "...")
            self._ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._ircsock.connect((server, port))
            ilogger.info("Connected to " + server + ". Sending bot info...")
            self.sender.change_socket(self._ircsock)
        self._restarting = False
        self.sender.queue(("USER " + username + " " + username + " " + server + " :" + realname, irc_send_thread.JOINING))
        self._retry = [0, 0]
        self.botnick = self.setnick()
    def run(self):
        global running

        joined = False # True if the bot has joined its channel
        waskicked = False # True if the bot has just been kicked out of its channel 

        silentcommands = (
            "004", # RPL_MYINFO
            "005", # RPL_ISUPPORT
            "250", # RPL_STATSDLINE
            "251", # RPL_LUSERCLIENT
            "252", # RPL_LUSEROP
            "253", # RPL_LUSERUNKNOWN
            "254", # RPL_LUSERCHANNELS
            "255", # RPL_LUSERME
            "265", # RPL_LOCALUSERS
        )
        nickproblems = {
            "431": "invalid", # ERR_NONICKNAMEGIVEN
            "432": "invalid", # ERR_ERRONEUSNICKNAME
            "433": "in use", # ERR_NICKNAMEINUSE
            "436": "in use", # ERR_NICKCOLLISION
            "437": "unavailable", # ERR_UNAVAILRESOURCE
        }

        self.pinger = irc_ping_thread(self)
        self.sender.start()
        self.pinger.start()

        ilogger.info("Starting IRC bot...")

        def joinchan(chan):
            ilogger.info("Joining " + chan + "...")
            self.sender.queue(("JOIN " + chan, irc_send_thread.INTERNAL))

        self.reconnect()

        self._ircsock.setblocking(False)
        while self._running:
            self._ircsock.settimeout(10.0)
            try:
                ircdata = self._ircsock.recv(4096) # receive data from the server
            except socket.timeout:
                continue
            except:
                ilogger.warn("Socket read error. Reconnecting.")
                self.reconnect()
                continue
            self._ircsock.settimeout(0.0)
            try:
                while True:
                    continuation = self._ircsock.recv(4096)
                    if not continuation:
                        break
                    ircdata += continuation
            except:
                pass
            self._ircsock.setblocking(True)
            if not ircdata:
                ilogger.warn("Socket seems to be closed. Reconnecting.")
                self.reconnect()
                continue
            try:
                ircdata = ircdata.decode("utf-8")
                ircdata = ircdata.replace("\r", "").strip("\n")
                messages = ircdata.split("\n")
            except:
                ilogger.error("Error while processing IRC message.", exc_info = 1)
                continue
            for ircmsg in messages:
                if not ircmsg:
                    continue
                try:
                    sender, senderuser, senderhost, command, chan, args, msg = parse_irc(ircmsg)
                    if command.isdigit():
                        targetuser, *args = args
                        if command in ("001", "002", "003"): # RPL_WELCOME, RPL_YOURHOST, RPL_CREATED
                            ilogger.info(msg)
                        if command == "001" and not joined: # RPL_WELCOME
                            self.sender.update_status(irc_send_thread.ACCEPTED)
                            ilogger.info("Joining channel...")
                            joinchan(channel)
                            joined = True
                        elif command in nickproblems:
                            problem = nickproblems[command]
                            self._retry[0] += 1
                            if self._retry[0] < len(botnicks):
                                ilogger.error("Desired username was %s.", problem)
                                self.botnick = self.setnick()
                            else:
                                self._retry[0] = 0
                                if self._retry[1] >= 6:
                                    ilogger.error("Desired username was %s. Aborting.", problem)
                                    self._leavereason = "Couldn't obtain desired nickname."
                                    self._running = False
                                else:
                                    ilogger.warn("Desired username was %s. Retrying in 10 seconds.", problem)
                                    time.sleep(10)
                                    self._retry[1] += 1
                                    self.botnick = self.setnick()
                        elif command == "311": # RPL_WHOISUSER
                            if len(args) >= 3:
                                ilogger.info(str(args))
                                self.whoisdata[irclower(args[0])].append("<b>%s</b> [%s@%s]" % (htmlencode(args[0]), htmlencode(args[1]), htmlencode(args[2])))
                            if len(args) >= 5:
                                self.whoisdata[irclower(args[0])].append("<b>Real name:</b> %s" % htmlencode(msg))
                        elif command == "312": # RPL_WHOISSERVER
                            if len(args) >= 3:
                                self.whoisdata[irclower(args[0])].append("<b>Server:</b> %s [%s]" % (htmlencode(args[1]), htmlencode(msg)))
                            elif len(args) >= 2:
                                self.whoisdata[irclower(args[0])].append("<b>Server:</b> %s" % htmlencode(args[1]))
                        elif command == "313": # RPL_WHOISOPERATO
                            if len(args) >= 1:
                                self.whoisdata[irclower(args[0])].append("<b>%s</b> is an IRC operator" % htmlencode(args[0]))
                        elif command == "317": # RPL_WHOISIDLE
                            if len(args) >= 2 and args[1].isdigit:
                                idlestart = time.strftime("%-d.%-m.%Y %H:%M:%S", time.localtime(time.time() - int(args[1])))
                                self.whoisdata[irclower(args[0])].append("<b>Idle since:</b> %s" % htmlencode(idlestart))
                        elif command == "318": # RPL_ENDOFWHOIS
                            if len(args) >= 1 and irclower(args[0]) in self.whoisdata and self.whoisdata[irclower(args[0])]:
                                tgmsg = "\n".join(self.whoisdata[irclower(args[0])])
                                tgthread.queue({
                                    "chat_id": tguser,
                                    "text": tgmsg,
                                    "parse_mode": "HTML",
                                })
                        elif command == "319": # RPL_WHOISCHANNELS
                            if len(args) >= 2:
                                self.whoisdata[irclower(args[0])].append("<b>Channels:</b> %s" % htmlencode(msg))
                        elif command == "353": # RPL_NAMREPLY
                            if len(args) >= 3:
                                channame = args[1]
                                msg = msg.replace("@", "@\u200d")
                                ilogger.info("Online on " + channame + ": " + msg)
                                tgmsg = "<b>Online on %s:</b> %s" % (htmlencode(channame), htmlencode(msg))
                                tgthread.queue({
                                    "chat_id": tguser,
                                    "text": tgmsg,
                                    "parse_mode": "HTML",
                                })
                        elif command == "366": # RPL_ENDOFNAMES
                            # todo send list here, at end of names
                            ilogger.info(msg)
                        elif command == "372": # RPL_MOTD
                            ilogger.info(msg)
                        elif command == "375": # RPL_MOTDSTART
                            ilogger.info("Start of MOTD")
                        elif command == "376": # RPL_ENDOFMOTD
                            ilogger.info("End of MOTD")
                        elif command == "401": # ERR_NOSUCHNICK
                            self.whoisdata[irclower(args[0])].append("No such nick: <b>%s</b>" % htmlencode(args[0]))
                        elif command == "402": # ERR_NOSUCHSERVER
                            ilogger.info("no server %s" % args[0])
                            self.whoisdata[irclower(args[0])].append("No such server: <b>%s</b>" % htmlencode(args[0]))
                        elif command == "474": # ERR_BANNEDFROMCHAN
                            ilogger.error("Banned from desired channel. Aborting.")
                            self._leavereason = "Couldn't join desired channel."
                            self._running = False
                        elif not command in silentcommands:
                            ilogger.info("Status %s: %s", command, msg)
                    else:
                        if command == "PRIVMSG":
                            ilogger.info("[" + args[0] + "] <" + sender + "> " + msg)
                            if msg.startswith("\x01ACTION") and msg.endswith("\x01"):
                                tgmsg = "%s</b> <i>%s</i>" % (htmlencode("* " + sender), htmlencode(msg[8:-1].lstrip()))
                            else:
                                tgmsg = "%s</b> %s" % (htmlencode("<" + sender + ">"), htmlencode(msg))
                            if chan == channel:
                                tgmsg = "<b>" + tgmsg
                            else:
                                tgmsg = "<b>%s " % htmlencode("[" + chan + "]") + tgmsg
                            possiblenicks = re.findall(r"[A-Za-z\[\]\\`_^{}|][A-Za-z\[\]\\`_^{}|0-9\-]+", msg)
                            ping = any(self.botnick in possiblenick for possiblenick in possiblenicks)
                            tgthread.queue({
                                "chat_id": tguser,
                                "text": tgmsg,
                                "parse_mode": "HTML",
                            }, ping = ping)
                        if command == "ERROR":
                            ilogger.info("Error: " + msg)
                        elif command == "JOIN" and channel in chan.split(","):
                            ilogger.info(sender + " joined " + chan)
                            tgmsg = "<b>%s</b> has joined <b>%s</b>" % (htmlencode(sender), htmlencode(chan))
                            tgthread.queue({
                                "chat_id": tguser,
                                "text": tgmsg,
                                "parse_mode": "HTML",
                            }, pingable = (self.botnick == sender))
                            if sender == self.botnick:
                                self.sender.update_status(irc_send_thread.JOINED)
                                if waskicked:
                                    waskicked = False
                        elif command == "PART" and channel in chan.split(","):
                            if len(args) > 1:
                                ilogger.info(sender + " left " + chan + " (" + msg + ")")
                                tgmsg = "<b>%s</b> has left <b>%s</b> (<i>%s</i>)" % (htmlencode(sender), htmlencode(chan), htmlencode(msg))
                            else:
                                ilogger.info(sender + " left " + chan)
                                tgmsg = "<b>%s</b> has left <b>%s</b>" % (htmlencode(sender), htmlencode(chan))
                            tgthread.queue({
                                "chat_id": tguser,
                                "text": tgmsg,
                                "parse_mode": "HTML",
                            }, pingable = (self.botnick == sender))
                        elif command == "QUIT":
                            if len(args) > 0:
                                ilogger.info(sender + " quit (" + msg + ")")
                                tgmsg = "<b>%s</b> has quit (<i>%s</i>)" % (htmlencode(sender), htmlencode(msg))
                            else:
                                ilogger.info(sender + " quit")
                                tgmsg = "<b>%s</b> has quit" % htmlencode(sender)
                            tgthread.queue({
                                "chat_id": tguser,
                                "text": tgmsg,
                                "parse_mode": "HTML",
                            }, pingable = (self.botnick == sender))
                        elif command == "KICK" and channel in chan.split(","):
                            kicked = args[1] if len(args) > 1 else "(unknown)"
                            if len(args) > 2:
                                ilogger.info(sender + " kicked " + kicked + " from " + chan + " (" + msg + ")")
                                tgmsg = "<b>%s</b> kicked <b>%s</b> from <b>%s</b> (<i>%s</i>)" % (htmlencode(sender), htmlencode(kicked), htmlencode(chan), htmlencode(msg))
                            else:
                                ilogger.info(sender + " kicked " + kicked + " from " + chan)
                                tgmsg = "<b>%s</b> kicked <b>%s</b> from <b>%s</b>" % (htmlencode(sender), htmlencode(kicked), htmlencode(chan))
                            tgthread.queue({
                                "chat_id": tguser,
                                "text": tgmsg,
                                "parse_mode": "HTML",
                            }, pingable = (self.botnick in [kicked, sender]))
                            if kicked == self.botnick:
                                ilogger.warn("Kicked from channel. Attempting to rejoin.")
                                tgthread.queue({
                                    "chat_id": tguser,
                                    "text": "<i>Kicked from channel. Attempting to rejoin.</i>",
                                    "parse_mode": "HTML",
                                })
                                waskicked = True
                                joinchan(chan)
                        elif command == "NICK":
                            ilogger.info(sender + " is now known as " + msg)
                            tgmsg = "<b>%s</b> is now known as <b>%s</b>" % (htmlencode(sender), htmlencode(msg))
                            tgthread.queue({
                                "chat_id": tguser,
                                "text": tgmsg,
                                "parse_mode": "HTML",
                            }, pingable = (self.botnick == sender))
                        elif command == "PING":
                            self.sender.queue(("PONG :" + msg, irc_send_thread.PING))
                        elif command == "PONG":
                            ilogger.info("Received PONG from server")
                            self.pinger.responded = True
                            self.sender.clear_drop_log()
                except:
                    ilogger.error("Error while processing IRC message.", exc_info = 1)
        self.sender.update_status(irc_send_thread.CONNECTED)
        self.kill_pinger()
        running = False
        self.sender.queue(("QUIT :" + self._leavereason, irc_send_thread.PING))
        # let the message be sent before disconnecting
        time.sleep(0.5)
        self.kill_sender()
        ilogger.info("Closing connection.")
        self._ircsock.close()
        self._ircsock = None

class photo_thread(threading.Thread):
    def __init__(self, photosize, text):
        super().__init__()
        self.daemon = True
        self._photosize = photosize
        self._text = text
    def run(self):
        try:
            file_id = self._photosize["file_id"]
            tlogger.error("Uploading image: %s" % file_id)
            tgmsg = "<i>Uploading image.</i>" if allowimages else "<i>Image uploading is disabled.</i>"
            message = bot.doRequest("sendMessage", {
                "chat_id": tguser,
                "text": tgmsg,
                "parse_mode": "HTML",
            })
            if not allowimages:
                return
            messageid = message["message_id"]
            try:
                details = bot.doRequest("getFile", {
                    "file_id": file_id,
                })
                path = details["file_path"]
                image = bot.getFile(path)
                extension = path[path.rindex("."):]
                hashed = hashlib.sha256(bytes(details["file_id"], "utf-8"))
                filepath = None
                while not filepath or os.path.exists(filepath):
                    filename = base64.b64encode(hashed.digest())[:6].decode("utf-8").replace("+", "-").replace("/", "_") + extension
                    filepath = imagepath + filename
                    hashed.update(bytes([random.choice(b"abcdefghjiklmnopqrstuvwxyz")]))
                tlogger.info("Saving image to %s" % filepath)
                with open(filepath, "wb") as file:
                    file.write(image)
                imageurl = urlpath + filename
                tlogger.info("Image uploaded to %s" % imageurl)
                if self._text is None:
                    ircmsg = imageurl
                elif "$$" in self._text:
                    ircmsg = self._text.replace("$$", imageurl, 1)
                else:
                    ircmsg = self._text + " " + imageurl
                ircthread.queue(ircmsg)
                tgmsg = "<b>%s</b> %s" % (htmlencode("<" + ircthread.botnick + ">"), htmlencode(ircmsg))
                message = bot.doRequest("editMessageText", {
                    "chat_id": tguser,
                    "message_id": messageid,
                    "text": tgmsg,
                    "parse_mode": "HTML",
                    "disable_web_page_preview": True,
                })
                tgthread.sender.ping_target = messageid
            except:
                tlogger.error("Error while uploading image.", exc_info = 1)
                message = bot.doRequest("editMessageText", {
                    "chat_id": tguser,
                    "message_id": messageid,
                    "text": "<i>Error uploading image.</i>",
                    "parse_mode": "HTML",
                })
        except:
            tlogger.error("Error while uploading image.", exc_info = 1)

class tg_thread(threading.Thread):
    def __init__(self):
        super().__init__()
        self.daemon = True
        self.sender = None
        self._running = True
    def queue(self, message, ping = None, pingable = False):
        if self.sender:
            self.sender.queue({"message": message, "ping": ping, "pingable": pingable})
    def please_die(self):
        self._running = False
    def kill_sender(self):
        self.sender.please_die()
        self.sender.join()
    def send_pings(self, implicit):
        sent = False
        for ping in self.sender.pings:
            if ping["ircid"] < self.sender.messageid - 5:
                tlogger.info("%s %s" % (ping["ircid"], self.sender.messageid))
                date = ping_timestamp(ping["time"])
                tgmsg = "<b>%s</b>" % date
                if ping["content"] is not None:
                    tgmsg += "\n" + ping["content"]
                self.queue({
                    "chat_id": tguser,
                    "text": tgmsg,
                    "reply_to_message_id": ping["tgid"],
                    "parse_mode": "HTML",
                })
            sent = True
        self.sender.pings.clear()
        if not sent and not implicit:
            self.queue({
                "chat_id": tguser,
                "text": "<i>No new pings.</i>",
                "parse_mode": "HTML",
            })
    def run(self):
        global running

        self.sender = tg_send_thread(bot)
        self.sender.daemon = True
        self.sender.start()

        while self._running:
            try:
                tlogger.info("Getting updates...")
                updates = bot.getUpdates(15)
                if not self._running:
                    break
                tlogger.info("Processing %d message(s)..." % len(updates))
                for update in updates:
                    if "message" in update:
                        target = update["message"]["chat"]["id"]
                        if target != tguser:
                            tlogger.info("Received message in %r which isn't %r", target, tguser)
                            continue
                        is_private = update["message"]["chat"]["type"] == "private"
                        if not is_private:
                            tlogger.info("Received message in a %s which isn't a private", update["message"]["chat"]["type"])
                            continue
                        if "from" not in update["message"]:
                            continue
                        user = update["message"]["from"]
                        sender_id = user["id"]
                        if sender_id != tguser:
                            tlogger.info("Received message from %r who isn't %r", sender_id, tguser)
                            continue
                        messageid = update["message"]["message_id"]
                        if "photo" in update["message"]:
                            largest = max(update["message"]["photo"], key = lambda size: size["width"] * size["height"])
                            photothread = photo_thread(largest, update["message"].get("caption"))
                            photothread.start()
                            continue
                        if "text" in update["message"]:
                            text = update["message"]["text"]
                            if text.startswith("/"):
                                command, *rest = text.split(None, 1)
                                rest = rest[0].lstrip() if rest else ""
                                if command == "/me":
                                    action = rest.lstrip()
                                    ircthread.queue("\x01ACTION %s\x01" % action)
                                    tgmsg = "<b>%s</b> <i>%s</i>" % (htmlencode("* " + ircthread.botnick), htmlencode(action))
                                    self.queue({
                                        "chat_id": tguser,
                                        "text": tgmsg,
                                        "parse_mode": "HTML",
                                    }, pingable = True)
                                elif command == "/shrug":
                                    shrug = "\u00af\u005c\u005f\u0028\u30c4\u0029\u005f\u002f\u00af"
                                    ircthread.queue(shrug)
                                    tgmsg = "<b>%s</b> %s" % (htmlencode("<" + ircthread.botnick + ">"), htmlencode(shrug))
                                    self.queue({
                                        "chat_id": tguser,
                                        "text": tgmsg,
                                        "parse_mode": "HTML",
                                    }, pingable = True)
                                elif command in ["/wi", "/whois"]:
                                    names = rest.split()
                                    ircthread.whoisdata[irclower(names[0])] = []
                                    ircthread.queue_raw("WHOIS " + names[0] + (" " + names[1] if len(names) > 1 else ""), irc_send_thread.USER)
                                elif command == "/wii":
                                    names = rest.split()
                                    ircthread.whoisdata[irclower(names[0])] = []
                                    ircthread.queue_raw("WHOIS " + names[0] + " " + names[0], irc_send_thread.USER)
                                elif command in ["/n", "/names"]:
                                    names = rest.split()
                                    ircthread.queue_raw("NAMES " + (names[0] if len(names) > 0 else channel), irc_send_thread.INTERNAL)
                                elif command in ["/p", "/pings"]:
                                    self.send_pings(False)
                                else:
                                    tlogger.info("Unknown command: %s" % text)
                                    self.queue({
                                        "chat_id": tguser,
                                        "text": "Unknown command.",
                                        "reply_to_message_id": messageid,
                                    })
                            else:
                                self.sender.ping_target = messageid
                                ircthread.queue(text)
                            self.send_pings(True)
            except telegram.ApiError as e:
                tlogger.error("Error while processing updates.", exc_info = 1)
            except IOError as e:
                tlogger.error("Error while processing updates.", exc_info = 1)

        running = False
        self.kill_sender()

running = True

tgthread = tg_thread()
ircthread = irc_thread()
tgthread.start()
ircthread.start()

try:
    while running:
        time.sleep(1)
except KeyboardInterrupt:
    pass

shutdown(True)
