import watchdog.observers, watchdog.events, logging

logger = logging.getLogger("AutoRestart")

class RestartHandler(watchdog.events.FileSystemEventHandler):
    def __init__(self, files, cleanup):
        super().__init__()
        self.files = files
        self.cleanup = cleanup
    def on_modified(self, event):
        import os, sys
        path = os.path.abspath(event.src_path)
        if self.files is None or path in self.files:
            type = "Directory " if event.is_directory else "File "
            logger.info(type + event.src_path + " was modified. Restarting.")
            self.cleanup()
            os.execl(sys.executable, sys.executable, *sys.argv)

def monitor(*files, path = None, cleanup = lambda: 0):
    import os
    if files:
        files = list(map(os.path.abspath, files))
    else:
        files = None
    if path is not None:
        path = os.path.dirname(os.path.abspath(path))
    elif not files:
        raise TypeError("at least one file or path is required")
    else:
        path = os.path.dirname(files[0])
    logger.info("Starting monitoring...")
    handler = RestartHandler(files, cleanup)
    observer = watchdog.observers.Observer()
    observer.schedule(handler, path, True)
    observer.start()
    logger.info("Now monitoring " + str(len(files)) + " file(s).")
